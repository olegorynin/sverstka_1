from flask import Flask, request, render_template

app = Flask(__name__)


@app.route('/my_page')
def start():
    return render_template("my_page.html")

